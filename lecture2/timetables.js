let tds = document.querySelectorAll('td:not(:empty)');
tds.forEach(td=>{
    td.onmouseover = ()=>{
      let moduleName = td.children[0].innerText;
      let tds = document.querySelectorAll('.'+moduleName);
      tds.forEach(e=>{
          e.classList.add('hi');
      });
    };
    td.onmouseout = ()=>{
      document.querySelectorAll('.hi').forEach(e=>{
          e.classList.remove('hi');
      });
    };
});



let d = document.createElement('button');
d.innerHTML = "Hide key";
document.body.prepend(d);
d.onclick = ()=>{
    let k = document.getElementById('key');
    if (k.classList.contains('hidden')){
        k.classList.remove('hidden');
        d.innerHTML = "Hide key";
    }else{
        k.classList.add('hidden');
        d.innerHTML = "Show key";
    }
}