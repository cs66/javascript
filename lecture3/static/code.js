fetch('static/zodiac.json')
  .then(r=>r.json())
  .then(r=>{
      let tgt = document.createElement('div');
      document.body.append(tgt);
      for(let i=0;i<r.length;i++){
          let z = r[i];
          let b = document.createElement('img');
          b.onclick = ()=>{
            tgt.innerHTML = `
            <table><caption>${z.name}</caption>
            <tr><td>Element</td><td>${z.element}</td></tr>
            <tr><td>Day</td><td>${z.day}</td></tr>
            <tr><td>Day</td><td><img src=${z.img}></td></tr>
            <tr><td>Colour</td><td style='background-color:${z.color}'>
              ${z.color}</td></tr>
            </table>
            `;
          };
          b.innerHTML = z.name;
          let x = 500;
          let y = 350; 
          let q = 300;
          b.src = z.img;
          b.style = `
          width:50px;
          position:absolute;
          left:${x+q*Math.cos(i*Math.PI/6)}px;
          top:${y+q*Math.sin(i*Math.PI/6)}px;`;
          document.body.append(b);
      }
  });
