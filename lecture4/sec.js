function pow(n,e,m){
    if (e<=0) return 1n;
    let h = e/2n;
    let r = pow(n,h,m);
    r = (r*r) % m;
    if (e % 2n === 1n){
        return (n * r) % m;
    }
    return r;
}

function modInverse(a, m){
  a = (a % m + m) % m
  if (!a || m < 2n) {
    return NaN // invalid input
  }
  // find the gcd
  const s = []
  let b = m
  while(b) {
    [a, b] = [b, a % b]
    s.push({a, b})
  }
  if (a !== 1n) {
    return NaN // inverse does not exists
  }
  // find the inverse
  let x = 1n
  let y = 0n
  for(let i = s.length - 2; i >= 0; --i) {
    [x, y] = [y,  x - y * (s[i].a / s[i].b)]
  }
  return (y % m + m) % m
}

document.getElementById('b1').onclick = ()=>{
    let a = BigInt(document.getElementById('a').value)
    let b = BigInt(document.getElementById('b').value)
    document.getElementById('op').value = pow(a,b,b);
}

document.getElementById('b2').onclick = ()=>{
    let p = BigInt(document.getElementById('p').value);
    let q = BigInt(document.getElementById('q').value);
    let e = BigInt(document.getElementById('e').value);
    let d = modInverse(e,(p-1n)*(q-1n));
    document.getElementById('d').value = d;
    document.getElementById('n').value = p*q;
}

document.getElementById('b3').onclick = ()=>{
    let e = BigInt(document.getElementById('e').value);
    let m = BigInt(document.getElementById('m').value);
    let n = BigInt(document.getElementById('n').value);
    document.getElementById('encrypted').value = pow(m,e,n);
}

document.getElementById('b4').onclick = ()=>{
    let c = BigInt(document.getElementById('encrypted').value);
    let d = BigInt(document.getElementById('d').value);
    let n = BigInt(document.getElementById('n').value);
    document.getElementById('dencrypted').value = pow(c,d,n);
}

