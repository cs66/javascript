import json
import urllib.request
w = json.load(open('worldl.json'))
for c in w:
    print('-- ' + c['name'])
    a = urllib.request.urlopen('https:'+c['flag'])
    open(f'flags/{c["tld"]}.svg','w', encoding="utf-8").write(a.read().decode('utf-8'))
fields = w[0].keys()
print("""
DROP TABLE world;
CREATE TABLE world(
    id INT PRIMARY KEY,
    name VARCHAR(50),
    continent VARCHAR(50),
    capital VARCHAR(50),
    area INT,
    population INT,
    gdp BIGINT,
    flag VARCHAR(100),
    tld VARCHAR(10)
);
""")
for c in w:
    print(f"""INSERT INTO world({",".join([f for f in fields])})
    VALUES ({",".join(["'"+str(c[f]).replace("'","''")+"'" for f in fields])});""")
