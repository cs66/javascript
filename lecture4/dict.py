mh = {
    "January":31,  "February":28, "March":31,    "April":30,
    "May":31,      "June":30,     "July":31,     "August":31,
    "September":30,"October":31,  "November":30, "December":31
}
thisMonth = "September"
print(f"This month, {thisMonth}, has {mh[thisMonth]} days")
print(f"Numbers of days: {[mh[k] for k in mh]}")
print(f"30 day months: {[k for k in mh if mh[k]==30]}")
print(f"There are {len(mh)} months")
print(f"Total days {sum(mh.values())}")
