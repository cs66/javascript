ml = [
    "January", "February", "March", "April",
    "May", "June", "July", "August",
    "September", "October", "November", "December"
]
print(f"There are {len(ml)} months")
print(f"Abbreviates to {''.join([m[0] for m in ml])}")
print(f"The number of letter {[len(m) for m in ml]}")
print(f"Months with r {[m for m in ml if 'r' in m]}")
