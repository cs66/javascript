name = "UK"
capital = "London"
continent = "Europe"
population = 66650000
area = 242495
print(f"The country {name} is in {continent}.")
print(f"{name} has a population of {population:,}")
print(f"Population density is  {population/area:.2f}")
